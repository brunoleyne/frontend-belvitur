import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Title from './Title';
import axios from 'axios'
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import API_URL from "../consts";

export default class AeroportosViagensProximas extends React.Component {
    state = {
        voos_proximos: []
    }

    componentDidMount() {
        axios.get(`${API_URL}/api/relatorio/voo/proximo`)
            .then(res => {
                const voos_proximos = res.data.voos_proximos;
                this.setState({voos_proximos});
            })
    }

    render() {
        return (
            <React.Fragment>
                <Title>Relação Aeroportos - Viagens mais próximas</Title>
                <Table size="small">
                    <TableHead>
                        <TableRow>
                            <TableCell>Origem</TableCell>
                            <TableCell>Destino</TableCell>
                            <TableCell>Distância (Km)</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.state.voos_proximos.map((row) => (
                            <TableRow>
                                <TableCell>{row.origem}</TableCell>
                                <TableCell>{row.destino}</TableCell>
                                <TableCell>{row.distancia_km}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </React.Fragment>
        )
    }
}