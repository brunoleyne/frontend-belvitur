import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Title from './Title';
import axios from 'axios'
import API_URL from "../consts";

export default class TopViagens extends React.Component {
  state = {
    viagens: []
  }

  componentDidMount() {
      axios.get(`${API_URL}/api/relatorio/top30`)
        .then(res => {
          const viagens = res.data.voos;
          this.setState({ viagens });
        })
  }

  render() {

    return (
        <React.Fragment>
          <Title>TOP 30 Viagens mais Longas</Title>
          <Table size="small">
            <TableHead>
              <TableRow>
                <TableCell>ID Voo</TableCell>
                <TableCell>Origem</TableCell>
                <TableCell>Destino</TableCell>
                <TableCell>Duração (Hrs)</TableCell>
                <TableCell>Distancia (Km)</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.state.viagens.map((row) => (
                  <TableRow key={row.id_voo}>
                    <TableCell>{row.id_voo}</TableCell>
                    <TableCell>{row.iata_from}</TableCell>
                    <TableCell>{row.iata_to}</TableCell>
                    <TableCell>{row.duracao}</TableCell>
                    <TableCell>{row.distancia_km}</TableCell>
                  </TableRow>
              ))}
            </TableBody>
          </Table>
        </React.Fragment>
    )
  }
}