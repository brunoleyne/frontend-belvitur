import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Title from './Title';
import axios from 'axios'
import API_URL from '../consts'

export default class EstadoAeroportos extends React.Component {
  state = {
      estados_aeroportos: []
  }

  componentDidMount() {
    axios.get(`${API_URL}/api/relatorio/estados`)
        .then(res => {
          const estados_aeroportos = res.data.estados_aeroportos;
          this.setState({ estados_aeroportos });
        })
  }

  render() {

    return (
        <React.Fragment>
          <Title>Relação Estado x Aeroporto</Title>
          <Table size="small">
            <TableHead>
              <TableRow>
                <TableCell>Estado</TableCell>
                <TableCell>Quantidade</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.state.estados_aeroportos.map((row) => (
                  <TableRow key={row.id_voo}>
                    <TableCell>{row.estado}</TableCell>
                    <TableCell>{row.quantidade}</TableCell>
                  </TableRow>
              ))}
            </TableBody>
          </Table>
        </React.Fragment>
    )
  }
}