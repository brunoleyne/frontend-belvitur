### Teste Tecnico Belvitur

- Frontend 16.3 - React + Material UI 4.11

## Instruções
Editar arquivo consts.js com a url da API Backend.

Executar os comandos:

Para instalar dependências:
```bash
$ npm install
```
Para subir o projeto em modo dev:
```bash
$ npm start
```

Executa em modo de desenvolvimento.<br />
Abrir [http://localhost:3000](http://localhost:3000) para visualizar no navegador.

## Modo Produção
```bash
$ npm run build
```

Arquivos disponíveis em /public